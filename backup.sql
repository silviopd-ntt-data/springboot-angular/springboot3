-- MySQL dump 10.13  Distrib 8.0.28, for macos12.0 (arm64)
--
-- Host: 127.0.0.1    Database: silviopd
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.8-MariaDB-1:10.5.8+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado` (
                            `empl_id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `direccion` varchar(255) DEFAULT NULL,
                            `email` varchar(255) DEFAULT NULL,
                            `nombre` varchar(255) DEFAULT NULL,
                            `telefono` varchar(255) DEFAULT NULL,
                            `id_empresa` bigint(20) DEFAULT NULL,
                            PRIMARY KEY (`empl_id`),
                            KEY `FKaph0gjj93d3xrkx7ixnua3hny` (`id_empresa`),
                            CONSTRAINT `FKaph0gjj93d3xrkx7ixnua3hny` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`empr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--


--
-- Table structure for table `empleado_rol`
--

DROP TABLE IF EXISTS `empleado_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado_rol` (
                                `empleado_id` bigint(20) NOT NULL,
                                `rol_id` bigint(20) NOT NULL,
                                KEY `FKqr0fmxw7eg3c90p8xc780wkad` (`rol_id`),
                                KEY `FK1fd76n8bh7dpt6sghe11rd8jt` (`empleado_id`),
                                CONSTRAINT `FK1fd76n8bh7dpt6sghe11rd8jt` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`empl_id`),
                                CONSTRAINT `FKqr0fmxw7eg3c90p8xc780wkad` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado_rol`
--


--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresa` (
                           `empr_id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `fecha_creacion` date DEFAULT NULL,
                           `razon_social` varchar(150) NOT NULL,
                           `representante` varchar(255) DEFAULT NULL,
                           `ruc` varchar(20) NOT NULL,
                           PRIMARY KEY (`empr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

INSERT INTO `empresa` VALUES (1,'2022-02-15','TEST EMPRESA 3','Juana Perez','09876543210987654323');

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
                       `id` bigint(20) NOT NULL AUTO_INCREMENT,
                       `nombre` varchar(255) DEFAULT NULL,
                       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--


--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `password` varchar(255) DEFAULT NULL,
                           `username` varchar(50) NOT NULL,
                           `empleado_id` bigint(20) DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `UK_863n1y3x0jalatoir4325ehal` (`username`),
                           KEY `FKjn24nnj8w0mmt2eq54ol6xrq2` (`empleado_id`),
                           CONSTRAINT `FKjn24nnj8w0mmt2eq54ol6xrq2` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`empl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-29 12:59:54
